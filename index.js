const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

app.get('/restart', (req, res) => {
    maxEventos = req.query.max;
    numero = 0;
    eventos = 0;
    res.sendFile(__dirname + '/index.html');
});

let maxEventos = 50;
let numero = 0;
let eventos = 0;
let jugadores = [];

io.on('connection', (socket) => {
    console.log('Se conectó un usuario');
    io.emit('numero', numero);
    io.emit('listaJugadores', jugadores);
    socket.on('sumar', () => {
        eventos++;
        if (eventos <= maxEventos) {
            numero++;
            io.emit('numero', numero);
        }
    });
    socket.on('restar', () => {
        eventos++;
        if (eventos <= maxEventos) {
            numero--;
            io.emit('numero', numero);
        }
    });
    socket.on('disconnect', () => {
        jugadores = jugadores.filter(({ nombre }) => nombre !== socket.nombre)
        io.emit('listaJugadores', jugadores);
        console.log(`${socket.nombre} se desconectó`);
    });
    socket.on('nombre', (nombre, equipo) => {
        socket.nombre = nombre;
        io.emit('listaJugadores', [...jugadores, { nombre, equipo }]);
        jugadores.push({ nombre, equipo });
    });
});

server.listen(port, () => {
    console.log(`Server started on port ${port}`);
});